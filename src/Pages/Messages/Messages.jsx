import React from 'react'
import { BiSolidPencil } from 'react-icons/bi'
import { BsPencilSquare } from 'react-icons/bs'
import MessageCard from '../../Components/Message/MessageCard'
import './Message.css'
import MessageContent from '../../Components/Message/MessageContent'

const Messages = () => {
  return (
    <div className=''>
        <div className=' flex w-[100%] px-7'>
            <div className='w-[37%] py-8'>
                <div className='flex items-center justify-between p-2'>
                    <div className='font-bold text-2xl'>Ikram.jouichi</div>
                    <div className='font-bold text-2xl mr-5'>
                        <BsPencilSquare/>
                    </div>
                </div>
                <div className='flex items-center justify-between mt-6'>
                    <div className='font-bold text-xl'>Messages</div>
                    <div className='font-semibold opacity-60 mr-5'>Demande</div>
                </div>
                <div className='listMessage'>
                    {[1,1,1,1,1,1,1,1,1].map(()=><MessageCard/>)}
                </div>
            </div>
            <div className='border h-[100vh]'></div>
            <div className='w-[65%]'>
             <MessageContent/>
            </div>
        </div>
      
    </div>
  )
}

export default Messages