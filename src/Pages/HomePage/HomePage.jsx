import React from 'react'
import StoryCircle from '../../Components/Story/StoryCircle'
import HomeRight from '../../Components/HomeRightPart/HomeRight'
import PostCard from '../../Components/Post/PostCard'
import CreatePostModal from '../../Components/Post/CreatePostModal'
import { useDisclosure } from '@chakra-ui/react'

const HomePage = () => {
  const { 
    isOpen, 
    onOpen, 
    onClose 
} = useDisclosure()
  return (
    <div className='py-5'>
      <div className='mt-10 flex w-[100%] justify-center'>
        <div className='w-[50%] px-4 '>
          <div className=' w-[50%] storyDiv flex space-x-2 rounded-md justify-start w-full '>
            {[1,1,1,11,1,1,1,1].map((item)=><StoryCircle/>)}
          </div>
          <div className='space-y-10 px-10 w-full mt-10'>
            {[1,1,1].map((item)=><PostCard/>)}
          </div>
        </div>
        <div className='w-[35%] ml-20 p-4'>
          <HomeRight/>
        </div>
      </div>
      
    </div>
  )
}

export default HomePage