import React from 'react'
import StoryViwer from '../../Components/StoryComponents/StoryViwer'

const Story = () => {
    const story=[
        {
            image:"https://images.pexels.com/photos/18091535/pexels-photo-18091535/free-photo-of-portret.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load"
        },
        {
            image:"https://images.pexels.com/photos/1109197/pexels-photo-1109197.jpeg?auto=compress&cs=tinysrgb&w=600"
        },
        {
            image:"https://images.pexels.com/photos/3747519/pexels-photo-3747519.jpeg?auto=compress&cs=tinysrgb&w=600"
        },
        {
            image:"https://images.pexels.com/photos/3933075/pexels-photo-3933075.jpeg?auto=compress&cs=tinysrgb&w=600"
        },
        {
            image:"https://images.pexels.com/photos/1142951/pexels-photo-1142951.jpeg?auto=compress&cs=tinysrgb&w=600"
        }
    ]
  return (
    <div>
        <StoryViwer stories={story}/>
    </div>
  )
}

export default Story