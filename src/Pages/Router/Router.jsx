import React from 'react'
import { Route, Routes } from 'react-router-dom'
import HomePage from '../HomePage/HomePage'
import Sidbar from '../../Components/Sidebar/Sidbar'
import Profile from '../Profile/Profile'
import Story from '../Story/Story'
import Reels from '../Reels/Reels'
import Messages from '../Messages/Messages'




const Router = () => {
  return (
    <div>
        <div className='flex'>
            <div className='sidebar' style={{ borderRight: '1px solid #ccc', paddingRight: '10px' }}>
              <Sidbar/>
            </div>
            <div className='w-full'>
              <Routes>
                  <Route path='/' element= {<HomePage/>}></Route>
                  <Route path='/username' element= {<Profile/>}></Route>
                  <Route path='/story' element= {<Story/>}></Route>
                  <Route path='/reels' element= {<Reels/>}></Route>
                  <Route path='/messages' element= {<Messages/>}></Route>
              </Routes>
            </div>
        </div>
    </div>
  )
}

export default Router