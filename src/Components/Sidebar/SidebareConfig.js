import {AiOutlineHome,AiFillHome, AiOutlineSearch, AiOutlineCompass, AiOutlineMessage, AiFillMessage, AiFillHeart} from 'react-icons/ai'
import {RiVideoLine,RiVideoFill} from 'react-icons/ri'
import {CgProfile} from 'react-icons/cg'
import { IoHeartOutline } from 'react-icons/io5'
import {BsPlusCircle, BsPlusCircleFill,BsPersonCircle} from 'react-icons/bs'
import {FaCompass,FaSearch} from 'react-icons/fa'
export const menu = [
    {title:"Home",icon:<AiOutlineHome className='text-2xl mr-5'></AiOutlineHome> , iactiveIcon: <AiFillHome className='text-2xl mr-5'></AiFillHome>},
    {title:"Search",icon:<AiOutlineSearch className='text-2xl mr-5'></AiOutlineSearch> , iactiveIcon: <FaSearch className='text-2xl mr-5'></FaSearch>},
    {title:"Explore",icon:<AiOutlineCompass className='text-2xl mr-5'></AiOutlineCompass> , iactiveIcon: <FaCompass className='text-2xl mr-5'></FaCompass>},
    {title:"Reels",icon:<RiVideoLine className='text-2xl mr-5'></RiVideoLine> , iactiveIcon: <RiVideoFill className='text-2xl mr-5'></RiVideoFill>},
    {title:"Message",icon:<AiOutlineMessage className='text-2xl mr-5'></AiOutlineMessage> , iactiveIcon: <AiFillMessage className='text-2xl mr-5'></AiFillMessage>},
    {title:"Notification",icon:<IoHeartOutline className='text-2xl mr-5'></IoHeartOutline> , iactiveIcon: <AiFillHeart className='text-2xl mr-5'></AiFillHeart>},
    {title:"Create",icon:<BsPlusCircle className='text-2xl mr-5'></BsPlusCircle>, iactiveIcon: <BsPlusCircleFill className='text-2xl mr-5'></BsPlusCircleFill>},
    {title:"Profile",icon:<CgProfile className='text-2xl mr-5'></CgProfile> , iactiveIcon: <BsPersonCircle className='text-2xl mr-5'></BsPersonCircle>}
]  