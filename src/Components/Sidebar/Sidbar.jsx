import React, { useState } from 'react'
import {IoReorderThreeOutline} from "react-icons/io5"
import { menu } from './SidebareConfig'
import { useNavigate } from 'react-router-dom';
import CreatePostModal from '../Post/CreatePostModal';
import { useDisclosure } from '@chakra-ui/react';
import SearchComponents from '../SearchComponents/SearchComponents';
//dans la ligne 56 j'ai supprimer les titres {item.title}

const Sidbar = () => {
    const { 
        isOpen, 
        onOpen, 
        onClose 
    } = useDisclosure()
const [isSearchVisible,setIsSearchVisible] = useState(false);   
const [activeTab,setActiveTab] = useState();
const navigate= useNavigate();
const handleTabClick = (title)=>{
    setActiveTab(title)
    if(title==="Profile"){
        navigate("/username")
    }
    else if (title==="Home"){
        navigate("/")
    }
    else if (title==="Reels"){
        navigate("/reels")
    }
    else if (title==="Message"){
        navigate("/messages")
    }
    else if (title==="Create"){
        navigate("/createpost");
        onOpen()

    }
    if (title==="Search"){
        setIsSearchVisible(true)
    }
    else{
        setIsSearchVisible(false)
    }
}

  return (
    <div className='sticky top-0 h-[100vh] flex'>
        <div className={`flex flex-col justify-between h-full ${activeTab==="Search"?"px-3":"px-10"}`}>
        {<div>
            { activeTab!=="Search" && <div className='pt-10'>
                <img className='w-40' src="https://www.oideya.gr.jp/wp-content/uploads/2022/01/insta-t.png" alt="" />
            </div>}
            <div className='mt-10  '>
                
                {menu.map((item)=> (<div onClick={()=>handleTabClick(item.title)} className='flex text-lg items-center mb-5 cursor-pointer '>
                    {activeTab===item.title? item.iactiveIcon : item.icon}
                    {activeTab!=="Search" && <p className= {`${ activeTab===item.title? "font-bold" : "font-semibold "}`}>{item.title}</p>}
                </div>
                ))}
            </div>
        </div>}
        <div className='flex items-center cursor-pointer pb-10'>
            <IoReorderThreeOutline className='text-2xl'/> 
            {activeTab!=="Search" && <p className='ml-5'>More</p>}
        </div>
        </div>
        <CreatePostModal onClose={onClose} isOpen={isOpen}/>
        {isSearchVisible && <SearchComponents/>}
    </div>
  )
}

export default Sidbar