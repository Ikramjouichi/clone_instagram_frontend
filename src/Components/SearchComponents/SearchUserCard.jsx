import React from 'react'

const SearchUserCard = () => {
  return (
    <div className='py-1 cursor-pointer'>
        <div className='flex items-center p-3'>
            <img className='w-12 h-12 rounded-full' src='https://cdn.pixabay.com/photo/2019/11/29/21/30/girl-4662158_1280.jpg' alt=''/>
            <div className='ml-3'>
                <p className='font-semibold'>Full Name</p>
                <p className='opacity-7à'>username</p>
            </div>
        </div>
    </div>
  )
}

export default SearchUserCard