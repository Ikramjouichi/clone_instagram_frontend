import React from 'react'

const MessageCard = () => {
  return (
    <div className='flex items-center justify-between  py-5 '>
        <div className='flex items-center cursor-pointer'>
            <div>
                <img className='w-12 h-12 rounded-full' src='https://media.istockphoto.com/id/1300324580/photo/young-pink-hair-girl-listening-music-in-headphones.jpg?s=1024x1024&w=is&k=20&c=9eKepxOaxdA1KRllzL7EnPdzIqWxyAIsNNbxC9PqZFs=' alt=''/>
            </div>
            <div className='ml-3'>
                <p>
                    <span className='font-semibold'>Full Name</span>
                </p>
                <div className='flex items-center space-x-3 text-xs opacity-60 pt-2'>
                    <span> vous:</span>
                    <span>good night</span>
                    <span>1 min</span>
                </div>
            </div>
        </div>
    </div>
  )
}

export default MessageCard