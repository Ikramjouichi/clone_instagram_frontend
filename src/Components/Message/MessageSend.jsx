import React from 'react'
import { BsEmojiSmile, BsThreeDots } from 'react-icons/bs'
import { HiOutlineArrowUturnLeft } from 'react-icons/hi2'
import './MessageSend.css'

const MessageSend = () => {
  return (
    <div className='flex items-center justify-end'>
        <div className='flex items-center'>
            <BsThreeDots className='ml-6 opacity-60 cursor-pointer'/>
            <HiOutlineArrowUturnLeft className='ml-6 opacity-60 cursor-pointer'/>
            <BsEmojiSmile className='ml-6 opacity-60 cursor-pointer'/>
        </div>
        <div className='messagesend ml-6'> i'am fine , thanks</div>
    </div>
  )
}

export default MessageSend