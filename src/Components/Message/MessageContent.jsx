import React from 'react'
import { BsCameraVideo, BsInfoCircle } from 'react-icons/bs'
import { IoCallOutline} from 'react-icons/io5'
import MessageSend from './MessageSend'
import MessageRecive from './MessageRecive'

const MessageContent = () => {
  return (
    <div>
        <div className='ml-10 py-6'>
            <div className='flex items-center text-3xl justify-between'>
                <img className='w-12 h-12 rounded-full' src='https://media.istockphoto.com/id/1300324580/photo/young-pink-hair-girl-listening-music-in-headphones.jpg?s=1024x1024&w=is&k=20&c=9eKepxOaxdA1KRllzL7EnPdzIqWxyAIsNNbxC9PqZFs='/>
                <div className='flex items-center'>
                <IoCallOutline/>
                <BsCameraVideo className='ml-4'/>
                <BsInfoCircle className='ml-4'/>
                </div>
            </div>
        </div>
        <hr className='w-full'/>
        <div className='ml-10 py-6 space-y-3'>
            {[1,1,1,1 ,1 ,1 ,1 ].map(()=><MessageRecive/>)}
            {[1,1,1,1 ,1 ,1 ,1 ].map(()=><MessageSend/>)}
            
        </div>
    </div>
  )
}

export default MessageContent