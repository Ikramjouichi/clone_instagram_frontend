import React from 'react'
import { BsEmojiSmile, BsThreeDots } from 'react-icons/bs'
import { HiOutlineArrowUturnLeft } from 'react-icons/hi2'
import './MessageSend.css'

const MessageRecive = () => {
  return (
    <div className='test flex items-center'>
        <img className='w-8 h-8 rounded-full ml-4' src='https://media.istockphoto.com/id/1300324580/photo/young-pink-hair-girl-listening-music-in-headphones.jpg?s=1024x1024&w=is&k=20&c=9eKepxOaxdA1KRllzL7EnPdzIqWxyAIsNNbxC9PqZFs='/>
        <div className='messagerecived ml-6'> i'am fine , thanks</div>
        <div className='flex items-center'>
            <BsEmojiSmile className='ml-6 opacity-60 cursor-pointer'/>
            <HiOutlineArrowUturnLeft className='ml-6 opacity-60 cursor-pointer'/>
            <BsThreeDots className='ml-6 opacity-60 cursor-pointer'/>
        </div>
    </div>
  )
}

export default MessageRecive