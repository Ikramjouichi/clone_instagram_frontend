import React from 'react'
import { AiFillHeart } from 'react-icons/ai'
import { FaComment } from 'react-icons/fa'
import './ReqUserPostCard.css'

const ReqUserPostCard = () => {
  return (
    <div className='p-2'>
        <div className='post h-60 w-60' >
            <img className='cursor-pointer' src='https://cdn.pixabay.com/photo/2023/09/08/15/49/ai-generated-8241456_1280.jpg' alt=''/>
            <div className='overlay' >
                <div className='overlay-text flex justify-between '>
                    <div>
                        <AiFillHeart/><span >10</span>
                    </div>
                    <div>
                        <FaComment/><span >10</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ReqUserPostCard