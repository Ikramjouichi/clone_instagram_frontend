import React from 'react'
import {TbCircleDashed} from 'react-icons/tb'

const ProfileUserDetails = () => {
  return (
    <div className='py-10 w-full '>
        <div className='flex items-center'>
            <div className='w-[15%]'>
                <img className='w-32 h-32 rounded-full ' src='https://cdn.pixabay.com/photo/2019/11/29/21/30/girl-4662158_1280.jpg' alt='' />
            </div>
            <div className='px-20 space-y-5'>
                <div className='flex space-x-10 items-center '>
                    <p>ikram.jouichi</p>
                    <button>Modifier Profil</button>
                    <button>Voir archives</button>
                    <TbCircleDashed></TbCircleDashed>
                </div>
                <div className='flex space-x-10 items-center'>
                    <div>
                        <span className='font-semibold mr-2'>10</span>
                        <span>publications</span>
                    </div>
                    <div>
                        <span className='font-semibold mr-2'>68</span>
                        <span>followers</span>
                    </div>
                    <div>
                        <span className='font-semibold mr-2'>90</span>
                        <span>suivi(e)s</span>
                    </div>
                </div>
                <div>
                    <p className='font-semibold'>ikram jouichi</p>
                    <p className='font-thin text-sm'>Engineering Grad 📚|🌹 A lady with a heart full of dreams 🌹 </p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ProfileUserDetails