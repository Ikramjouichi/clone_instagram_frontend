import React from 'react'
import {Modal,ModalOverlay,ModalContent,ModalBody} from '@chakra-ui/react'
import { BsBookmark, BsBookmarkFill, BsEmojiSmile, BsThreeDots } from 'react-icons/bs'
import {  AiFillHeart, AiOutlineHeart } from 'react-icons/ai'
import { FaRegComment } from 'react-icons/fa'
import { RiSendPlaneLine} from 'react-icons/ri'


const ReelThreeDotsModal = ({onClose,isOpen,isSaved,isPostLiked,handlePostSaved,handlePostLike}) => {
  return (
    <div>
        <Modal size={"5xl"} onClose={onClose} isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>
          </ModalBody>
        </ModalContent>
      </Modal>
    </div>
  )
}


export default ReelThreeDotsModal