import React, { useState } from 'react'
import './ReelCard.css'
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai'
import { FaRegComment } from 'react-icons/fa';
import { RiSendPlaneLine } from 'react-icons/ri';
import { BsBookmark, BsBookmarkFill, BsThreeDots } from 'react-icons/bs';
import { useDisclosure } from '@chakra-ui/react';
import CommentModal from '../Comment/CommentModal';
import ReelThreeDotsModal from './ReelThreeDotsModal';
// import ReactPlayer from 'react-player'
//mt-10 flex w-[100%] h-[100%] justify-center
const ReelCard = () => {
  //for likes state
  const [isPostLiked,setIsPostLiked] = useState(false);
  const handlePostLike= ()=>{
      setIsPostLiked(!isPostLiked);
  }
  //for saved state
  const [isSaved, setIsSaved] = useState(false);
  const handlePostSaved=()=>{
      setIsSaved(!isSaved);
  }
  //for Comment Modal
  const { 
    isOpen, 
    onOpen, 
    onClose 
} = useDisclosure()
const handleOpenCommentModal=()=>{
    onOpen()
}

const handleReelDots=()=>{
  onOpen()
}
const [isMondal,setIsMondal]= useState()
const handleMondal=()=>{
   
}
  return (
    <div className='container-un py-2'>
      <div className='container-deux mt-10 flex justify-center'>
          <div className="videoCard justify-center flex">
              <video className="reel-video">
                  <source
                  src="https://cdn.pixabay.com/vimeo/414754673/sous-marin-37712.mp4?width=640&hash=d040813ba90a85cc90ebce5dfb2d18ab78f2afbc"
                  type="video/mp4"
                  />
              </video>
              <div className='px-4'>
                  <div className=' space-y-8  bottom-0 start-100 translate-middle-x px-4'>
                      <div className='py-16'></div>
                      <div className='py-16'></div>
                      <div>
                        {isPostLiked? <AiFillHeart onClick={handlePostLike} className='text-3xl  cursor-pointer text-red-600'/>
                                  :<AiOutlineHeart onClick={handlePostLike} className='text-3xl hover:opacity-50 cursor-pointer'/> }
                                  <p>Like</p>
                      </div>
                      <div>
                        <FaRegComment onClick={handleOpenCommentModal} className='text-2xl hover:opacity-50 cursor-pointer'/>
                        <p>312</p>
                      </div>
                      <div>
                        <RiSendPlaneLine className='text-2xl hover:opacity-50 cursor-pointer'/>
                      </div>
                      <div>
                        {isSaved?<BsBookmarkFill onClick={handlePostSaved} className='text-2xl cursor-pointer'/>
                              :<BsBookmark onClick={handlePostSaved} className='text-2xl hover:opacity-50 cursor-pointer'/>}
                      </div>
                      <div>
                        <BsThreeDots  onClick={handleReelDots} className='text-2xl hover:opacity-50 cursor-pointer'/>
                      </div>
                  </div>
                  <div>
                      <CommentModal
                          handlePostLike={handlePostLike} 
                          handlePostSaved={handlePostSaved} 
                          isPostLiked={isPostLiked} 
                          isSaved={isSaved} 
                          isOpen={isOpen} 
                          onClose={onClose}
                      />
                  </div>
                  <div>
                      <ReelThreeDotsModal
                          handlePostLike={handlePostLike} 
                          handlePostSaved={handlePostSaved} 
                          isPostLiked={isPostLiked} 
                          isSaved={isSaved} 
                          isOpen={isOpen} 
                          onClose={onClose}
                      />
                  </div>
              </div>
          </div>
      </div>
      
    </div>
  )
}

export default ReelCard