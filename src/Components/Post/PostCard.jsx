import React, { useState } from 'react'
import { BsBookmark, BsBookmarkFill, BsEmojiSmile, BsThreeDots } from 'react-icons/bs'
import {  AiFillHeart, AiOutlineHeart } from 'react-icons/ai'
import { FaRegComment } from 'react-icons/fa'
import { RiSendPlaneLine} from 'react-icons/ri'
import './PostCard.css'
import CommentModal from '../Comment/CommentModal'
import { useDisclosure } from '@chakra-ui/react';


const PostCard = () => {
    const [showDropDown,setshowDropDown]= useState(false);
    const handleClick =()=>{
        setshowDropDown(!showDropDown);
    }
    //for likes state
    const [isPostLiked,setIsPostLiked] = useState(false);
    const handlePostLike= ()=>{
        setIsPostLiked(!isPostLiked);
    }
    //for saved state
    const [isSaved, setIsSaved] = useState(false);
    const handlePostSaved=()=>{
        setIsSaved(!isSaved);
    }
    //for Comment Modal
    const { 
        isOpen, 
        onOpen, 
        onClose 
    } = useDisclosure()
    const handleOpenCommentModal=()=>{
        onOpen()
    }
  return (
    <div>
        <div className='border rounded-md w-full'>
            <div className='flex justify-between items-center w-full py-4 px-5'>
                <div className='flex items-center'>
                    <img className='h-12 w-12 rounded-full' src='https://cdn.pixabay.com/photo/2019/11/29/21/30/girl-4662158_1280.jpg' alt=''/>
                    <div className='pl-2'>
                        <p className='font-semibold text-sm '>username</p>
                        <p className='font-thin text-sm'>location</p>
                    </div>
                </div>
                <div className='dropdown'>
                    <BsThreeDots className='dots' onClick={handleClick} />
                    <div onClick={handleClick} className='dropdown-content'>
                        {showDropDown && <p className='bg-black text-white py-1 px-4 rounded-md cursor-pointer'>Delete</p>}
                    </div>
                </div>
            </div>
            <div className='w-full'>
                <img className='w-full' src='https://cdn.pixabay.com/photo/2023/02/04/21/32/flowers-7768218_1280.jpg' alt=''/>
            </div>
            <div className='flex justify-between items-center w-full px-5 py-4'>
                <div className='flex items-center space-x-2'>
                    {isPostLiked? <AiFillHeart onClick={handlePostLike} className='text-2xl  cursor-pointer text-red-600'/>
                                :<AiOutlineHeart onClick={handlePostLike} className='text-2xl hover:opacity-50 cursor-pointer'/> }
                    <FaRegComment onClick={handleOpenCommentModal} className='text-xl hover:opacity-50 cursor-pointer'/>
                    <RiSendPlaneLine className='text-xl hover:opacity-50 cursor-pointer'/>
                </div>
                <div>
                    {isSaved?<BsBookmarkFill onClick={handlePostSaved} className='text-xl cursor-pointer'/>
                            :<BsBookmark onClick={handlePostSaved} className='text-xl hover:opacity-50 cursor-pointer'/>}
                </div>
            </div>

            <div className='w-full px-5 py-2'>
                <p>10 likes</p>
                <p className='opacity-50 cursor-pointer py-2'>view all 10 comments</p>
            </div>
            <div className='w-full px-5 py-2'>
                <div className='flex w-full items-center justify-between'>
                        <input className='commentInput' type='text' placeholder='Add a comment...' />
                        <BsEmojiSmile className='opacity-50 cursor-pointer'/>
                </div>
            </div>
        </div>
        <div>
            <CommentModal 
                handlePostLike={handlePostLike} 
                handlePostSaved={handlePostSaved} 
                isPostLiked={isPostLiked} 
                isSaved={isSaved} 
                isOpen={isOpen} 
                onClose={onClose}
            />
        </div>
    </div>
  )
}

export default PostCard