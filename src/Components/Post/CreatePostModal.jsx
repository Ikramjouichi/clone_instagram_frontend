import React, { useState } from 'react'
import {Modal,ModalOverlay,ModalContent,ModalBody,Button,ModalCloseButton} from '@chakra-ui/react'
import { FaPhotoVideo } from 'react-icons/fa'
import './CreatePostModal.css'
import { GrEmoji } from 'react-icons/gr'
import { GoLocation } from 'react-icons/go'

const CreatePostModal = ({
    onClose,isOpen
}) => {

    const [isDragOver,setIsDragOver]= useState(false);
    const [file, SetFile] = useState();
    const [caption, SetCaption] = useState("");

    const handleDrop=(event)=>{
        console.log("je suis dans handleDrop() ")
        event.preventDefault()
        const droppedFile= event.dataTransfer.file[0];
        if(droppedFile.type.startsWith("image/") || droppedFile.type.startsWith("video/")){
            console.log("je suis dans handleDrop() dans if ")
            SetFile(droppedFile)
        }
    }
    const handleDragOver=(event)=>{
        console.log("je suis dans handleDragOver() debut ")
        event.preventDefault();
        event.dataTransfer.dropEffect="copy";
        setIsDragOver(true)
        console.log("je suis dans handleDragOver() fin ")
    }
    const handleDragLeave=()=>{
        console.log("je suis dans handleDragLeave() debut ")
        setIsDragOver(false)
        console.log("je suis dans handleDragLeave() fin ")
    }
    const handleOnChange =(e)=>{
        console.log("je suis dans handleOnchange() debut")
        const file=e.target.files[0];
        console.log("je suis dans handleOnchange() ")
        if(file && (file.type.startsWith("image/") || file.type.startsWith("video/"))){
            console.log("je suis dans handleOnchange() dans if avant setFile()",file)
            SetFile(file);
        }
        else{
            console.log("je suis dans handleOnchange() dans else avant setFile()")
            SetFile(null);
            console.log("je suis dans handleOnchange() dans if apres setFile()")
            alert("please select an image or video")
            
        }
    }
    const handleCaptionChange=(e)=>{
        SetCaption(e.target.value)
    }
  return (
    <div>
        <Modal size={'4xl'} onClose={onClose} isOpen={isOpen} isCentered>
            <ModalOverlay />
            <ModalContent>
            <div className='flex items-center justify-between px-10 py-1'>
                <p className='text-xl font-semibold'>Create new Post</p>
                <Button className='' variant={'ghost'} size='sm' colorScheme={'blue'} >share</Button>
            </div>
            <ModalCloseButton/>
            <hr/>
            <ModalBody>
                <div className='h-[70vh] justify-between pb-5 flex'>
                    <div className='w-[50%]'>
                        {!file && <div
                        onDrop={handleDrop}
                        onDragOver={handleDragOver}
                        onDragLeave={handleDragLeave}
                        className='drag-drop h-full'
                        >
                            <div>
                                <FaPhotoVideo className='text-3xl'/>
                                <p>Drag Photos or Videos here</p>
                            </div>
                            <label htmlFor="file-upload" className='custom-file-upload'>Select from Computer</label>
                            <input className='fileInput' type="file" id='file-upload' accept='image/*,video/*' onChange={handleOnChange}/>
                        </div>}
                        {file && <img className='h-[70vh] max-h-full' src={URL.createObjectURL(file)} alt=''/>}
                    </div>
                    <div className='w-[1px] border h-full'></div>
                    <div className='w-[50%]'>
                        <div className='flex items-center px-2'>
                            <img className='w-12 h-12 rounded-full' src='https://cdn.pixabay.com/photo/2023/07/19/15/47/honeybee-8137242_1280.jpg' alt=''/>
                            <p className='font-semibold ml-4'>username</p>
                        </div>
                        <div className='px-2'>
                            <textarea placeholder='write a caption' onClick={handleCaptionChange} className='captionInput' name='caption' rows="8"></textarea>
                        </div>
                        <div className='flex justify-between px-2'>
                            <GrEmoji/>
                            <p className='opacity-70'>{caption?.length} /2,200</p>
                        </div>
                        <hr/>
                        <div className=' flex p-2 justify-between items-center'>
                            <input type='text' className='locationInput' placeholder='Add location' name='location'/>
                            <GoLocation/>
                        </div>
                        <hr/>
                    </div>
                </div>
            </ModalBody>
            
            </ModalContent>
        </Modal>
    </div>
  )
}

export default CreatePostModal