import React from 'react'
import { useNavigate } from 'react-router-dom'

const StoryCircle = () => {
  const navigate = useNavigate()
  const handleNavigate=()=>{
    navigate("/story")
  }
  return (
    <div onClick={handleNavigate} className='cursor-pointer flex flex-col items-center '>
        <img className='w-16 h-16 rounded-full' src='https://cdn.pixabay.com/photo/2016/11/29/03/36/woman-1867093_1280.jpg' alt='' />
        <p className=''>username</p>
    </div>
  )
}

export default StoryCircle