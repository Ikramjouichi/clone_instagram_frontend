import React from 'react'
import SuggetionCard from './SuggetionCard'

const HomeRight = () => {
  return (
    <div className=''>
      <div>
        <div className='flex items-center justify-between'>
          <div className='flex items-center'>
            <div>
              <img className='w-12 h-12 rounded-full' src='https://cdn.pixabay.com/photo/2019/11/29/21/30/girl-4662158_1280.jpg' alt=''/>
            </div>
            <div className='ml-3'>
              <p >full name</p>
              <p className='opacity-70'>username</p>
            </div>
          </div>
          <div>
            <p className='text-blue-500 text-sm font-semibold' >Switch</p>
          </div>
        </div>
        <div className='flex justify-between items-center space-y-5 mt-10'>
          <p className='font-bold opacity-50'>Suggetion for you</p>
          <p className='font-semibold'>See all</p>
        </div>
        <div className='space-y-5 mt-4'>
              {[1,1,1,1,1,1].map(()=><SuggetionCard/>)}
        </div>
      </div>
    </div>
  )
}

export default HomeRight