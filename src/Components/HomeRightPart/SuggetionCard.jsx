import React from 'react'

const SuggetionCard = () => {
  return (
    <div className='flex items-center justify-between '>
        <div className='flex items-center'>
            <img className='w-9 h-9 rounded-full' src='https://cdn.pixabay.com/photo/2016/11/29/03/36/woman-1867093_1280.jpg' alt=''/>
            <div className='ml-2'>
                <p className='text-sm font-semibold '>username</p>
                <p className='text-sm font-semibold opacity-70'>fllows you</p>
            </div>
        </div>
        <p className='text-blue-500 text-sm font-semibold'>Fllow</p>
    </div>
  )
}

export default SuggetionCard