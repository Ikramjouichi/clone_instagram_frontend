import React from 'react'
import {Modal,ModalOverlay,ModalContent,ModalBody} from '@chakra-ui/react'
import CommentCard from './CommentCard'
import { BsBookmark, BsBookmarkFill, BsEmojiSmile, BsThreeDots } from 'react-icons/bs'
import {  AiFillHeart, AiOutlineHeart } from 'react-icons/ai'
import { FaRegComment } from 'react-icons/fa'
import { RiSendPlaneLine} from 'react-icons/ri'
import './CommentModal.css'


const CommentModal = ({onClose,isOpen,isSaved,isPostLiked,handlePostSaved,handlePostLike}) => {
  return (
    <div>
        <Modal size={"5xl"} onClose={onClose} isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>
            <div className='flex h-[85vh] relative'>
                <div className='w-[45%] flex flex-col justify-center '>
                    <img className='max-h-full w-full' src='https://cdn.pixabay.com/photo/2019/09/13/03/59/saxophone-4473023_1280.jpg' alt=''/>
                </div>
                <div className='b w-[55%] pl-10'>
                    <div className='flex justify-between items-center py-5 '>
                        <div className='flex items-center'>
                            <div>
                                <img className='w-11 h-11 rounded-full' src='https://media.istockphoto.com/id/1250515365/photo/beautiful-woman-in-white-headphones-listens-to-music.jpg?s=1024x1024&w=is&k=20&c=CjHrt8QkhJQhzJZ3uUm0xpDiHGg0Uv2iZUfEeZjNAuU=' alt=''/>
                            </div>
                            <div className='ml-4'>
                                <p>username</p>
                            </div>
                        </div>
                        <BsThreeDots/>
                    </div>
                    <hr/>
                    <div className='comment' >
                        {[1,1,1,1,1,1,1].map(()=><CommentCard/>)}
                    </div>
                    <div className=' absolute bottom-0 w-[50%]'> 
                        <div className='flex justify-between items-center w-full  py-4'>
                            <div className='flex items-center space-x-2'>
                                {isPostLiked? <AiFillHeart onClick={handlePostLike} className='text-2xl  cursor-pointer text-red-600'/>
                                            :<AiOutlineHeart onClick={handlePostLike} className='text-2xl hover:opacity-50 cursor-pointer'/> }
                                <FaRegComment className='text-xl hover:opacity-50 cursor-pointer'/>
                                <RiSendPlaneLine className='text-xl hover:opacity-50 cursor-pointer'/>
                            </div>
                            <div>
                                {isSaved?<BsBookmarkFill onClick={handlePostSaved} className='text-xl cursor-pointer'/>
                                        :<BsBookmark onClick={handlePostSaved} className='text-xl hover:opacity-50 cursor-pointer'/>}
                            </div>
                        </div>
                        <div className='w-full py-2'>
                            <p className=''>10 likes</p>
                            <p className='opacity-50 text-sm'>1 day ago</p>
                        </div>
                        <div className='w-full py-3'>
                            <div className='flex w-full items-center justify-between'>
                                    <input className='commentInput' type='text' placeholder='Add a comment...' />
                                    <BsEmojiSmile className='opacity-50 cursor-pointer'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </ModalBody>
        </ModalContent>
      </Modal>
    </div>
  )
}

export default CommentModal